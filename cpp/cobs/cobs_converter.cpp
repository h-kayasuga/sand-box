#include "cobs_converter.h"

uint32_t encode_cobs(const uint8_t *data, uint32_t size, uint8_t *buffer) {
    uint8_t *ptr_buffer = buffer;       //!< array for output
    uint8_t count = 1;                  //!< non zero counter
    uint8_t *ptr_zero = ptr_buffer++;   //!< pointer to store zero point

    for (uint32_t i = 0; i < size; i++) {
        uint8_t byte = data[i];
        if (byte) {
            // non zero value
            // append value and update buffer index
            *ptr_buffer++ = byte;
            // update counter
            count++;
        }
        if (!byte || (count == 0xFF)) {
            // value is zero or block end
            // insert count to previous zero point
            *ptr_zero = count;
            // reset counter
            count = 1;
            // store(update) zero point
            ptr_zero = ptr_buffer;
            // update buffer index
            ptr_buffer++;
        }
    }
    return uint32_t(ptr_buffer - buffer);
}

uint32_t decode_cobs(const uint8_t *buffer, uint32_t size, uint8_t *data) {
    return 0;   
}