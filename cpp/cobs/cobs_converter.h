#include <stdint.h>

uint32_t encode_cobs(const uint8_t *data, uint32_t size, uint8_t *buffer);
uint32_t decode_cobs(const uint8_t *buffer, uint32_t size, uint8_t *data);