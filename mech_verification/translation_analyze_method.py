#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pandas as pd
import matplotlib.pyplot as plt
from numpy import float64
import numpy as np

MASTER_DISTANCE = 22.5    # mm

X = 0
Y = 1
Z = 2
TOPIC_NAME = ['x', 'y', 'z']
TOPIC_UNIT = 'mm'
TRIGGER_SPEED_THRESHOLD = 5.0 # mm

# 判定計算
def process(ref_df, state_df, optotrak_df, scale, arm_topic):
  if arm_topic != '/log_scope':
    TRACKING_THRESHOLD = 3.9 # mm
    SPEED_THRESHOLD = 80.0    # mm/s
  else:
    TRACKING_THRESHOLD = 5.6 # mm
    SPEED_THRESHOLD = 40.0    # mm/s
  ret = pd.Series(data=None, dtype=float64)
  # トラッキング精度
  ret['tracking_threshold'] = TRACKING_THRESHOLD
  # トラッキング計算
  for i in range(len(TOPIC_NAME)):
    ret['tracking_' + TOPIC_NAME[i]] = (ref_df[TOPIC_NAME[i]] - optotrak_df[TOPIC_NAME[i]]).abs().max()
  # 判定（1つでも閾値以上の場合はFalse）
  tracking_result = True
  for i in range(len(TOPIC_NAME)):
    if ret['tracking_' + TOPIC_NAME[i]] > TRACKING_THRESHOLD:
      tracking_result = False
  if tracking_result:
    ret['tracking_result'] = 'Success'
  else:
    ret['tracking_result'] = 'Fail'
  # 最高速さ
  ret['speed_threshold'] = SPEED_THRESHOLD
  for i in range(len(TOPIC_NAME)):
    ret['speed_' + TOPIC_NAME[i]] = optotrak_df['velocity_' + TOPIC_NAME[i]].abs().max()
  # 判定（1つでも閾値以下の場合はFalse）
  speed_result = True
  for i in range(len(TOPIC_NAME)):
    if ret['speed_' + TOPIC_NAME[i]] < SPEED_THRESHOLD:
      speed_result = False
  if speed_result:
    ret['speed_result'] = 'Success'
  else:
    ret['speed_result'] = 'Fail'
  # スケーリング
  ret['distance_min'] = MASTER_DISTANCE * scale[0] - TRACKING_THRESHOLD
  ret['distance_max'] = MASTER_DISTANCE * scale[0] + TRACKING_THRESHOLD
  ret['distance_ref'] = np.sqrt(np.square(ref_df['x'].max() - ref_df['x'].min()) \
                                + np.square(ref_df['y'].max() - ref_df['y'].min()) \
                                + np.square(ref_df['z'].max() - ref_df['z'].min()))
  if (ret['distance_min'] < ret['distance_ref']) and (ret['distance_ref'] < ret['distance_max']):
    ret['scaling_result'] = 'Success'
  else:
    ret['scaling_result'] = 'Fail'
  ret['distance_measure'] = np.sqrt(np.square(optotrak_df['x'].max() - optotrak_df['x'].min()) \
                                    + np.square(optotrak_df['y'].max() - optotrak_df['y'].min()) \
                                    + np.square(optotrak_df['z'].max() - optotrak_df['z'].min()))
  return ret, ref_df, state_df, optotrak_df

# 描写
def plot(ref_df, state_df, optotrak_df, file_name):
  # トラッキング
  fig = plt.figure()
  for i in range(len(TOPIC_NAME)):
    ax = fig.add_subplot(int(100 * len(TOPIC_NAME) + 11 + i), xlabel='t[s]', ylabel=TOPIC_NAME[i] + '[' + TOPIC_UNIT + ']')
    ax.plot(ref_df['Time'], ref_df[TOPIC_NAME[i]], label='ref', color ='red')
    ax.plot(state_df['Time'], state_df[TOPIC_NAME[i]], label='state', color ='blue')
    ax.plot(optotrak_df['Time'], optotrak_df[TOPIC_NAME[i]], label='optotrak', color ='green')
    ax.grid(b=True, which='major')
    ax.legend(bbox_to_anchor=(1,1), loc='upper right', borderaxespad=0, fontsize=10)
  fig.savefig(file_name +'_track.png')
  plt.close()
  # トラッキング誤差(vs optotrak)
  fig = plt.figure()
  ax = fig.add_subplot(int(111), xlabel='t[s]', ylabel='error' + '[' + TOPIC_UNIT + ']')
  for i in range(len(TOPIC_NAME)):
    error = (ref_df[TOPIC_NAME[i]] - optotrak_df[TOPIC_NAME[i]]).abs()
    ax.plot(optotrak_df['Time'], error, label='error_' + TOPIC_NAME[i])
  ax.grid(b=True, which='major')
  ax.legend(bbox_to_anchor=(1,1), loc='upper right', borderaxespad=0, fontsize=10)
  fig.savefig(file_name +'_error_ref_optotrak.png')
  plt.close()
  # トラッキング誤差(vs state)
  fig = plt.figure()
  ax = fig.add_subplot(int(111), xlabel='t[s]', ylabel='error' + '[' + TOPIC_UNIT + ']')
  for i in range(len(TOPIC_NAME)):
    error = (ref_df[TOPIC_NAME[i]] - state_df[TOPIC_NAME[i]]).abs()
    ax.plot(optotrak_df['Time'], error, label='error_' + TOPIC_NAME[i])
  ax.grid(b=True, which='major')
  ax.legend(bbox_to_anchor=(1,1), loc='upper right', borderaxespad=0, fontsize=10)
  fig.savefig(file_name +'_error_ref_state.png')
  plt.close()
  # 速度
  fig = plt.figure()
  for i in range(len(TOPIC_NAME)):
    ax = fig.add_subplot(int(100 * len(TOPIC_NAME) + 11 + i), xlabel='t[s]', ylabel=TOPIC_NAME[i] + '[' + TOPIC_UNIT + '/s]')
    ax.plot(ref_df['Time'], ref_df['velocity_' + TOPIC_NAME[i]], label='ref', color ='red')
    ax.plot(state_df['Time'], state_df['velocity_' + TOPIC_NAME[i]], label='state', color ='blue')
    ax.plot(optotrak_df['Time'], optotrak_df['velocity_' + TOPIC_NAME[i]], label='optotrak', color ='green')
    ax.grid(b=True, which='major')
    ax.legend(bbox_to_anchor=(1,1), loc='upper right', borderaxespad=0, fontsize=10)
  fig.savefig(file_name +'_velocity.png')
  # 速さ
  fig = plt.figure()
  ax = fig.add_subplot(int(111), xlabel='t[s]', ylabel='speed [' + TOPIC_UNIT + '/s]')
  ax.plot(ref_df['Time'], ref_df['speed'], label='ref', color ='red')
  ax.plot(state_df['Time'], state_df['speed'], label='state', color ='blue')
  ax.plot(optotrak_df['Time'], optotrak_df['speed'], label='optotrak', color ='green')
  ax.grid(b=True, which='major')
  ax.legend(bbox_to_anchor=(1,1), loc='upper right', borderaxespad=0, fontsize=10)
  fig.savefig(file_name +'_speed.png')
  plt.close()
  plt.close()

# 全体誤差計算
def get_total_error(ref_df, optotrak_df):
  error_list = []
  for i in range(len(TOPIC_NAME)):
    error = (ref_df[TOPIC_NAME[i]] - optotrak_df[TOPIC_NAME[i]]).abs().sum()
    error_list.append(error)
  return max(error_list)
