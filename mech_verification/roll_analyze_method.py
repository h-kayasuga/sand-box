#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pandas as pd
import matplotlib.pyplot as plt
from numpy import float64
import math

DEG_TO_RAD = math.pi / 180
SMOOTH_THRESHOLD = 1.0e-9  # rad/s

TOPIC_NAME = ['roll']
TOPIC_UNIT = 'rad'
TRIGGER_SPEED_THRESHOLD = 5.0 * DEG_TO_RAD # deg/s

# トラッキング、最高速度、スムース
def process(ref_df, state_df, optotrak_df, scale, arm_topic):
  if arm_topic != '/log_scope':
    TRACKING_THRESHOLD = 1.95 / ((17.5 + 7.8) * math.sin(80.0 * DEG_TO_RAD)) # rad
    SPEED_THRESHOLD = 2.0   # rad/s
  else:
    TRACKING_THRESHOLD = 15 * DEG_TO_RAD # rad
    SPEED_THRESHOLD = 2.0   # rad/s
  ret = pd.Series(data=None, dtype=float64)
  # トラッキング精度
  ret['tracking_threshold'] = TRACKING_THRESHOLD
  # トラッキング計算
  for i in range(len(TOPIC_NAME)):
    ret['tracking_' + TOPIC_NAME[i]] = (ref_df[TOPIC_NAME[i]] - optotrak_df[TOPIC_NAME[i]]).abs().max()
  # 判定（1つでも閾値以上の場合はFalse）
  tracking_result = True
  for i in range(len(TOPIC_NAME)):
    if ret['tracking_' + TOPIC_NAME[i]] > TRACKING_THRESHOLD:
      tracking_result = False
  if tracking_result:
    ret['tracking_result'] = 'Success'
  else:
    ret['tracking_result'] = 'Fail'
  # 最高速さ
  ret['speed_threshold'] = SPEED_THRESHOLD
  for i in range(len(TOPIC_NAME)):
    ret['speed_' + TOPIC_NAME[i]] = optotrak_df['velocity_' + TOPIC_NAME[i]].abs().max()
  # 判定（1つでも閾値以下の場合はFalse）
  speed_result = True
  for i in range(len(TOPIC_NAME)):
    if ret['speed_' + TOPIC_NAME[i]] < SPEED_THRESHOLD:
      speed_result = False
  if speed_result:
    ret['speed_result'] = 'Success'
  else:
    ret['speed_result'] = 'Fail'
  # スムース
  ret['smooth_threshold'] = SMOOTH_THRESHOLD
  # 非停止時インデックス取得
  no_stop_index_list = []
  for i in range(len(TOPIC_NAME)):
    no_stop_index_list.append(ref_df.index[ref_df['velocity_' + TOPIC_NAME[i]].abs() > SMOOTH_THRESHOLD])
  # 判定（1つでも閾値以下の場合はFalse）
  smooth_result = True
  for i in range(len(TOPIC_NAME)):
    ret['smooth' + TOPIC_NAME[i]] = optotrak_df.loc[no_stop_index_list[i]]['velocity_' + TOPIC_NAME[i]].abs().min()
    if ret['smooth' + TOPIC_NAME[i]] < SMOOTH_THRESHOLD:
      smooth_result = False
  if smooth_result:
    ret['smooth_result'] = 'Success'
  else:
    ret['smooth_result'] = 'Fail'
  return ret, ref_df, state_df, optotrak_df

# 描写
def plot(ref_df, state_df, optotrak_df, file_name):
  # トラッキング
  fig = plt.figure()
  for i in range(len(TOPIC_NAME)):
    ax = fig.add_subplot(int(100 * len(TOPIC_NAME) + 11 + i), xlabel='t[s]', ylabel=TOPIC_NAME[i] + '[' + TOPIC_UNIT + ']')
    ax.plot(ref_df['Time'], ref_df[TOPIC_NAME[i]], label='ref', color ='red')
    ax.plot(state_df['Time'], state_df[TOPIC_NAME[i]], label='state', color ='blue')
    ax.plot(optotrak_df['Time'], optotrak_df[TOPIC_NAME[i]], label='optotrak', color ='green')
    ax.grid(b=True, which='major')
    ax.legend(bbox_to_anchor=(1,1), loc='upper right', borderaxespad=0, fontsize=10)
  fig.savefig(file_name +'_track.png')
  plt.close()
  # トラッキング誤差(vs optotrak)
  fig = plt.figure()
  ax = fig.add_subplot(int(111), xlabel='t[s]', ylabel='error' + '[' + TOPIC_UNIT + ']')
  for i in range(len(TOPIC_NAME)):
    error = (ref_df[TOPIC_NAME[i]] - optotrak_df[TOPIC_NAME[i]]).abs()
    ax.plot(optotrak_df['Time'], error, label='error_' + TOPIC_NAME[i])
  ax.grid(b=True, which='major')
  ax.legend(bbox_to_anchor=(1,1), loc='upper right', borderaxespad=0, fontsize=10)
  fig.savefig(file_name +'_error_ref_optotrak.png')
  plt.close()
  # トラッキング誤差(vs state)
  fig = plt.figure()
  ax = fig.add_subplot(int(111), xlabel='t[s]', ylabel='error' + '[' + TOPIC_UNIT + ']')
  for i in range(len(TOPIC_NAME)):
    error = (ref_df[TOPIC_NAME[i]] - state_df[TOPIC_NAME[i]]).abs()
    ax.plot(optotrak_df['Time'], error, label='error_' + TOPIC_NAME[i])
  ax.grid(b=True, which='major')
  ax.legend(bbox_to_anchor=(1,1), loc='upper right', borderaxespad=0, fontsize=10)
  fig.savefig(file_name +'_error_ref_state.png')
  plt.close()
  # 速度
  fig = plt.figure()
  for i in range(len(TOPIC_NAME)):
    ax = fig.add_subplot(int(100 * len(TOPIC_NAME) + 11 + i), xlabel='t[s]', ylabel=TOPIC_NAME[i] + '[' + TOPIC_UNIT + '/s]')
    ax.plot(ref_df['Time'], ref_df['velocity_' + TOPIC_NAME[i]], label='ref', color ='red')
    ax.plot(state_df['Time'], state_df['velocity_' + TOPIC_NAME[i]], label='state', color ='blue')
    ax.plot(optotrak_df['Time'], optotrak_df['velocity_' + TOPIC_NAME[i]], label='optotrak', color ='green')
    ax.grid(b=True, which='major')
    ax.legend(bbox_to_anchor=(1,1), loc='upper right', borderaxespad=0, fontsize=10)
  fig.savefig(file_name +'_velocity.png')
  plt.close()

# 全体誤差計算
def get_total_error(ref_df, optotrak_df):
  return (ref_df['roll'] - optotrak_df['roll']).abs().sum()