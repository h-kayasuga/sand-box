#!/usr/bin/env python3

import PySimpleGUI as sg
from analyze_method import *

class AnalyzeGui():

    def __init__(self):
        sg.theme('Black')

        # GUI Layout
        arm_topic_list = ['/log_right', '/log_left', '/log_scope']
        analyze_method_list = ['translation', 'roll']

        layout = [[sg.Text('Rosbag'), sg.InputText(size=(100,1)), sg.FileBrowse(key='-Rosbag-', file_types=(('Rosbag','*.bag'),))],
                  [sg.Text('Optotrak'), sg.InputText(size=(100,1)), sg.FileBrowse(key='-Optotrak-', file_types=(('Optotrak','*RF*.csv'),))],
                  [sg.Text('Log'), sg.InputText(size=(1,1)), sg.FolderBrowse(key='-Log-')],
                  [sg.Combo(arm_topic_list, default_value='/log_left', size=(10,1), key='-ArmTopic-'),
                   sg.Combo(analyze_method_list, default_value='translation', size=(10,1), key='-AnalyzeMethod-'), sg.Button('Analyze', key='-Analyze-')]]
        self.window = sg.Window('Analyze', layout)

        # Event loop
        while True:
            self.event, self.values = self.window.read()

            if self.event in (sg.WIN_CLOSED, None):
                break
            elif self.event == '-Analyze-':
                rosbag = self.values['-Rosbag-']
                optotrak = self.values['-Optotrak-']
                arm_topic = self.values['-ArmTopic-']
                method = self.values['-AnalyzeMethod-']
                log_folder = self.values['-Log-']
                if not rosbag or not optotrak:
                    if not log_folder:
                        sg.popup('Cancel', 'No file supplied.')
                    else:
                        log_list = get_translation_log_pair(log_folder)
                        print(log_list)
                        for log_pair in log_list:
                            self.process(log_pair[0], arm_topic, log_pair[1], method)
                else:
                    self.process(rosbag, arm_topic, optotrak, method)
        self.window.close()

    def process(self, rosbag, arm_topic, optotrak, method):
        ret = analyze_log(rosbag, arm_topic, optotrak, os.path.dirname(rosbag), method, True)
        ret.to_csv(os.path.splitext(rosbag)[0] + '_' + method + '_RESULT.csv')
        print('!!! Complete !!!')

if __name__ == '__main__':
    gui = AnalyzeGui()
