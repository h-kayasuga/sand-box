#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import math
import pandas as pd
import numpy as np
from numpy import float64
import subprocess
import glob

import roll_analyze_method as roll
import translation_analyze_method as translation

OPTOTRAK_FREQUENCY = 100  # Hz
SLAVE_FREQUENCY = 100     # Hz
UR5_FREQUENCY = 500       # Hz
MIN_FREQUENCY = min([SLAVE_FREQUENCY, OPTOTRAK_FREQUENCY, UR5_FREQUENCY])
MARKER_1_2_LENGTH = 11.0    # mm
MARKER_2_TIP_LENGTH = 16.7  # mm
M_TO_MM = 1000
RAD_TO_DEG = 180 / math.pi
DEG_TO_RAD = math.pi / 180
S_TO_MS = 1000
MS_TO_S = 1.0 / S_TO_MS
TOPIC_NAME = ['x', 'y', 'z', 'roll']

# Rosbag+Optotrak解析
def analyze_log(rosbag, arm_topic, optotrak, dir_name, method, is_plot=False):
  # methodの選択
  if method == 'translation':
    method_class = translation
  else:
    method_class = roll
  # DataFrameの作成
  ref_df, state_df, optotrak_df, scale, wirst_scale = create_dateframe(rosbag, arm_topic, optotrak)
  # 全体波形から追従遅れ計算
  delay_index = 8#stimate_tracking_delay(ref_df, optotrak_df, method)
  print(delay_index / OPTOTRAK_FREQUENCY * S_TO_MS)
  # plotする場合フォルダ作成
  plot_dir_name = dir_name + '/' + os.path.splitext(os.path.basename(rosbag))[0] + '_delay_' + str(int(delay_index / OPTOTRAK_FREQUENCY * S_TO_MS)) + 'ms_plot'
  if is_plot:
    os.makedirs(plot_dir_name, exist_ok=True)
  # データ分割
  trigger_list = make_trigger_index_from_slavelog_speed(ref_df, 'speed', method_class.TRIGGER_SPEED_THRESHOLD)
  if (trigger_list == []) or (len(trigger_list) < 5):
    trigger_list = make_trigger_up_down_index_from_slavelog(ref_df, 'x')
  print(trigger_list)
  # 解析
  ret = pd.DataFrame(data=None, dtype=float64)
  for i, trigger in enumerate(trigger_list):
    # 区間抽出
    ref_df_part = ref_df[trigger[0]: trigger[1]].reset_index()
    state_df_part = state_df[trigger[0]: trigger[1]].reset_index()
    optotrak_part = optotrak_df[trigger[0] + delay_index: trigger[1] + delay_index].reset_index()

    # 要素数不一致
    if len(ref_df_part) != len(optotrak_part):
      continue

    # 平均値オフセット
    ref_df_part = remove_mean_offset(ref_df_part)
    state_df_part = remove_mean_offset(state_df_part)
    optotrak_part = remove_mean_offset(optotrak_part)

    # 時間オフセット
    ref_df_part['Time'] = ref_df_part['Time'] - ref_df_part['Time'][0]
    state_df_part['Time'] = state_df_part['Time'] - state_df_part['Time'][0]
    optotrak_part['Time'] = optotrak_part['Time'] - optotrak_part['Time'][0]

    # 計算
    series, ref_df_part, state_df_part, optotrak_part = method_class.process(ref_df_part, state_df_part, optotrak_part, scale, arm_topic)
    ret = ret.append(series, ignore_index=True)

    # プロット
    if is_plot:
      method_class.plot(ref_df_part, state_df_part, optotrak_part, plot_dir_name + '/plot_' + str(i))
  return ret

def rosbag_to_df(rosbag, topic, save_as_csv=False):
  tmp_file = os.path.splitext(rosbag)[0] + topic.replace('/', '_') + '.csv'
  print(tmp_file)
  if not os.path.exists(tmp_file):
    # ros > csv変換
    command = '. /opt/ros/kinetic/setup.sh;'
    command += 'rostopic echo -b ' + rosbag + ' -p ' + topic + ' > ' + tmp_file
    subprocess.call(command, shell=True)
  df = pd.read_csv(tmp_file, header=0)
  if not save_as_csv:
    os.remove(tmp_file)
  # 時刻修正
  return modify_rosbag_time(df)

def modify_rosbag_time(rosbag_df):
  rosbag_df.insert(0, 'Time', 0)
  if 'field.header.stamp' in rosbag_df.columns:
    rosbag_df['Time'] = (rosbag_df['field.header.stamp'] - rosbag_df['field.header.stamp'][0]) * 1.0e-9
  elif '%time' in rosbag_df.columns:
    rosbag_df['Time'] = (rosbag_df['%time'] - rosbag_df['%time'][0]) * 1.0e-9
  return rosbag_df

def optotrak_to_df(optotrak_csv):
  print(optotrak_csv)
  tmp_file = './tmp_file.csv'
  # OptoTrakのファイルかどうかチェック
  with open(optotrak_csv, 'r') as f:
      line = f.readline()
      if line.find('Number of frames') < 0:
          return pd.DataFrame(data=None, dtype=float64)
  # Optotrakデータの4行目の凡例の数が足りない場合にはカンマを追加して一致させる
  with open(optotrak_csv, 'r') as f:
      lines = f.readlines()
      if lines[4].count(',') != lines[5].count(','):
          lines[4] = lines[4][:-1] + ",\r\n"
  with open(tmp_file, 'w') as f:
      f.writelines(lines)
  # CSV読み込み
  df = pd.read_csv(tmp_file, header=3)
  os.remove(tmp_file)
  # 時刻修正
  df.insert(0, 'Time', 0)
  OPTOTRAK_DELAY = -0.43
  df['Time'] = (df['Frame'] - 1) / OPTOTRAK_FREQUENCY + OPTOTRAK_DELAY
  df = df[df['Time'] > 0.0].reset_index()
  return df

def make_trigger_up_down_index_from_slavelog(df, key):
  threshold = df[key].mean()
  trigger_up = []
  pre_state = (df[key][0] >= threshold)
  for i in range(len(df[key])):
    now_state = (df[key][i] >= threshold)
    # 状態がFalseからTrueに変化した場合、トリガー判定
    if (not pre_state) and now_state:
      trigger_up.append(i)
    pre_state = now_state
  trigger = []
  for i in range(len(trigger_up) - 1):
    trigger.append([trigger_up[i], trigger_up[i + 1] - 1])
  return trigger

def make_trigger_index_from_slavelog_speed(df, key, threshold):
  lock_detect = True
  # 速さ0検出
  speed_zero = []
  for i in range(len(df[key])):
    if (df[key][i] < threshold) and not lock_detect:
      # 速度0検出
      speed_zero.append(i)
      lock_detect = True
    elif df[key][i] > threshold and lock_detect:
      # 速度検出
      lock_detect = False

  trigger_list = []
  for i in range(int(len(speed_zero) / 2) - 1):
    trigger_list.append([speed_zero[::2][i], speed_zero[::2][i + 1]])
  return trigger_list

def remove_mean_offset(df):
  df_dummy = df.copy()
  for name in df_dummy:
    if name in TOPIC_NAME:
      df_dummy[name] = df[name] - df[name].mean()
  return df_dummy

# 要素を抽出したDataFrameの作成（最低周波数でリサンプリング）
def create_dateframe(rosbag, arm_topic, optotrak):
  print('Make DataFrame')
  slave_df = rosbag_to_df(rosbag, arm_topic, True)
  print('Finish Arm Topic')
  optotrak_df = optotrak_to_df(optotrak)
  print('Finish Optotrak')
  if arm_topic == '/log_scope':
    cal_scope_pos(optotrak_df)
  else:
    cal_tip_pos(optotrak_df)
  cal_tip_roll(optotrak_df)
  ref_df = pd.DataFrame({ 'Time' : slave_df['Time'],
                          'x' : slave_df['field.variable.ref.tip_trans_ref.translation.x'] * M_TO_MM,
                          'y' : slave_df['field.variable.ref.tip_trans_ref.translation.y'] * M_TO_MM,
                          'z' : slave_df['field.variable.ref.tip_trans_ref.translation.z'] * M_TO_MM,
                          'roll' : slave_df['field.variable.ref.holder.q_ref5']})[::int(SLAVE_FREQUENCY / MIN_FREQUENCY)].reset_index()
  state_df = pd.DataFrame({ 'Time' : slave_df['Time'],
                            'x' : slave_df['field.variable.state.tip_trans.translation.x'] * M_TO_MM,
                            'y' : slave_df['field.variable.state.tip_trans.translation.y'] * M_TO_MM,
                            'z' : slave_df['field.variable.state.tip_trans.translation.z'] * M_TO_MM,
                            'roll' : slave_df['field.variable.state.holder.q5']})[::int(SLAVE_FREQUENCY / MIN_FREQUENCY)].reset_index()
  optotrak_df = pd.DataFrame({'Time' : optotrak_df['Time'],
                              'x' : optotrak_df['opto_inst_tip_x'],
                              'y' : optotrak_df['opto_inst_tip_y'],
                              'z' : optotrak_df['opto_inst_tip_z'],
                              'roll' : optotrak_df['opto_inst_roll']})[::int(OPTOTRAK_FREQUENCY / MIN_FREQUENCY)].reset_index()
  # 速度計算
  for topic in TOPIC_NAME:
    ref_df['velocity_' + topic] = ref_df.diff()[topic] / ref_df.diff()['Time']
    state_df['velocity_' + topic] = state_df.diff()[topic] / state_df.diff()['Time']
    optotrak_df['velocity_' + topic] = optotrak_df.diff()[topic] / optotrak_df.diff()['Time']
  # 並進速さ
  ref_df['speed'] = np.sqrt(np.square(ref_df['velocity_x']) + np.square(ref_df['velocity_y']) + np.square(ref_df['velocity_z']))
  state_df['speed'] = np.sqrt(np.square(state_df['velocity_x']) + np.square(state_df['velocity_y']) + np.square(state_df['velocity_z']))
  optotrak_df['speed'] = np.sqrt(np.square(optotrak_df['velocity_x']) + np.square(optotrak_df['velocity_y']) + np.square(optotrak_df['velocity_z']))
  # スケーリング
  pos_scale = slave_df['field.variable.common.position_scale']
  wrist_scale = slave_df['field.variable.common.position_scale']
  return ref_df, state_df, optotrak_df, pos_scale, wrist_scale

# Optotrakインスツルメント位置計算
def cal_tip_pos(optotrak_df):
  # Optotrakの座標 --->>>
  # Marker#1
  m1x = optotrak_df['Marker_1 x']
  m1y = optotrak_df['Marker_1 y']
  m1z = optotrak_df['Marker_1 z']
  # Marker#2
  m2x = optotrak_df['Marker_2 x']
  m2y = optotrak_df['Marker_2 y']
  m2z = optotrak_df['Marker_2 z']
  ratio = (MARKER_2_TIP_LENGTH + MARKER_1_2_LENGTH) / MARKER_1_2_LENGTH
  # 回転中心X
  center_x = ratio * (m2x - m1x) + m1x
  # 回転中心Y
  center_y = ratio * (m2y - m1y) + m1y
  # 回転中心z
  center_z = ratio * (m2z - m1z) + m1z
  # <<<--- Optotrakの座標
  # 座標変換
  optotrak_df['opto_inst_tip_x'] = - center_x
  optotrak_df['opto_inst_tip_y'] = - center_z
  optotrak_df['opto_inst_tip_z'] = - center_y

# Optotrakスコープ位置計算
def cal_scope_pos(optotrak_df):
  # Optotrakの座標 --->>>
  # Marker#13 or #2
  mx = optotrak_df['Marker_13 x']
  my = optotrak_df['Marker_13 y']
  mz = optotrak_df['Marker_13 z']
  if (mx.isnull().all()) or (my.isnull().all()) or (mz.isnull().all()):
    mx = optotrak_df['Marker_2 x']
    my = optotrak_df['Marker_2 y']
    mz = optotrak_df['Marker_2 z']
  # <<<--- Optotrakの座標
  # 座標変換
  optotrak_df['opto_inst_tip_x'] = - mx
  optotrak_df['opto_inst_tip_y'] = - mz
  optotrak_df['opto_inst_tip_z'] = - my

# Optotrakロール角度計算
def cal_tip_roll(optotrak_df):
  # Marker#13
  m13x = optotrak_df['Marker_13 x']
  m13y = optotrak_df['Marker_13 y']
  m13z = optotrak_df['Marker_13 z']
  # Marker#14
  m14x = optotrak_df['Marker_14 x']
  m14y = optotrak_df['Marker_14 y']
  m14z = optotrak_df['Marker_14 z']
  # Marker#15
  m15x = optotrak_df['Marker_15 x']
  m15y = optotrak_df['Marker_15 y']
  m15z = optotrak_df['Marker_15 z']
  # Marker#16
  m16x = optotrak_df['Marker_16 x']
  m16y = optotrak_df['Marker_16 y']
  m16z = optotrak_df['Marker_16 z']
  # 基準ベクトル計算
  base_roll_vec = np.array([m14x[0] - m15x[0], m14y[0] - m15y[0], m14z[0] - m15z[0]])  # 回転の基準となるベクトル
  base_cal_vec = np.array([m13x[0] - m15x[0], m13y[0] - m15y[0], m13z[0] - m15z[0]])   # 同一平面内にある回転軸ベクトル計算用ベクトル
  base_rotate_vec = np.cross(base_roll_vec, base_cal_vec)                              # 回転軸ベクトル

  roll_list = [0 for i in range(len(optotrak_df))]
  for i in range(len(optotrak_df)):
    if i == 0:
      roll_list[0] = 0
    else:
      roll_vec = np.array([m14x[i] - m15x[i], m14y[i] - m15y[i], m14z[i] - m15z[i]])  # 回転の基準となるベクトル
      rotate_vec = np.cross(base_roll_vec, roll_vec)                                  # 回転軸ベクトル
      # 内積計算
      # 回転量
      cos_theta = np.dot(base_roll_vec, roll_vec) / (np.linalg.norm(base_roll_vec) * np.linalg.norm(roll_vec))
      theta = math.acos(cos_theta)
      # 回転方向
      cos_dir =  np.dot(base_rotate_vec, rotate_vec) / (np.linalg.norm(base_rotate_vec) * np.linalg.norm(rotate_vec))
      if cos_dir > 0:
        theta = - theta
      roll_list[i] = theta
  optotrak_df['opto_inst_roll'] = roll_list

# トラッキング遅れ推定
def estimate_tracking_delay(ref_df, optotrak_df, method):
  ref_df_offset = remove_mean_offset(ref_df)
  optotrak_df_offset = remove_mean_offset(optotrak_df)
  # 追従遅れ ms
  tracking_delay_ms_list = [t for t in range(0, 330, int(1.0 / OPTOTRAK_FREQUENCY * S_TO_MS))]
  min_index = 0
  min_error = 1e9
  error_list = []
  for tracking_delay_ms in tracking_delay_ms_list:
    tracking_index = int(tracking_delay_ms * MS_TO_S * OPTOTRAK_FREQUENCY)
    optotrak_shift_df = optotrak_df_offset.copy().shift( - tracking_index)
    error = 0
    if method == 'roll':
      error = roll.get_total_error(ref_df_offset, optotrak_shift_df)
    elif method == 'translation':
      error = translation.get_total_error(ref_df_offset, optotrak_shift_df)
    error_list.append(error)
    if error < min_error:
      min_error = error
      min_index = tracking_index
  print(error_list)
  return min_index

# フォルダ下のRosbagとOptotrakを出力
def get_translation_log_pair(folder):
  log_list = []
  key_list = ['a', 'b', 'c', 'd', 's', 't', 'u', 'v','No', 'uu', 'no']
  for key in key_list:
    for i in range(100):
      rosbag_list = glob.glob(folder + '/' + key + str(i) + '*.bag')
      optotrak_list = glob.glob(folder + '/' + key + str(i) + '*RF*.csv')
      if (len(rosbag_list) == 1) and (len(optotrak_list) == 1):
        log_list.append([rosbag_list[0], optotrak_list[0]])
  return log_list