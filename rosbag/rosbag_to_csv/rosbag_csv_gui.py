#!/usr/bin/env python3
# -*- coding: utf-8 -*- 
import PySimpleGUI as sg
import os
import threading

class RosConverterGui():
    def __init__(self):
        sg.theme('Black')
        # GUI Layout
        layout = [[sg.Text('File(s)'), sg.InputText(enable_events=True, key='-FileInput-'), sg.FilesBrowse(key='-File-', file_types=(('Rosbag','*.bag'),))],
                  [sg.Text('Folder'), sg.InputText(enable_events=True, key='-FolderInput-'), sg.FolderBrowse(key='-Folder-')],
                  [sg.Text('Topic'), sg.Combo([], size=(40,1), key='-Topic-'), sg.Button('Convert', key='-Convert-')]]
        self.window = sg.Window('Converter', layout)
        self.file_name = ''
        self.folder_name = ''
        # Event loop
        while True:
            event, values = self.window.read()
            if event in (sg.WIN_CLOSED, None):
                break
            elif event == '-Convert-':
                files = values['-FileInput-'].split(';')
                folder = values['-FolderInput-']
                topic = values['-Topic-']
                if files != '' and topic != '':
                    print('Start Files')
                    for file in files:
                        print(file)
                        threading.Thread(target=rosbag_to_csv, args=(file, topic)).start()
                    thread_list = threading.enumerate()
                    thread_list.remove(threading.main_thread())
                    for thread in thread_list:
                        thread.join()
                    print('End')
                elif folder != '' and topic != '':
                    print('Start Folder')
                    file_list = baglist_from_folder(folder)
                    for file in file_list:
                        print(file)
                        threading.Thread(target=rosbag_to_csv, args=(file, topic)).start()
                    thread_list = threading.enumerate()
                    thread_list.remove(threading.main_thread())
                    for thread in thread_list:
                        thread.join()
                    print('End')
                sg.popup('End')
            if values['-FileInput-'] != '':
                file = values['-FileInput-'].split(';')[0]
                if file != self.file_name:
                    self.window['-FolderInput-'].update('')
                    self.file_name = file
                    topics = get_topic_from_rosbag(file)
                    self.window['-Topic-'].update(values=topics)
            if values['-FolderInput-'] != '':
                folder = values['-FolderInput-']
                if folder != self.folder_name:
                    self.window['-FileInput-'].update('')
                    self.folder_name = folder
                    file_list = baglist_from_folder(folder)
                    topics = get_topic_from_rosbag(file_list[0])
                    self.window['-Topic-'].update(values=topics)
        self.window.close()

import platform
import subprocess
# --->>> Rosbag Method
def get_topic_from_rosbag(rosbag):
    command = ''

    # for windows(wsl)
    pf = platform.system()
    if pf == 'Windows':
        rosbag = winpath_to_wslpath(rosbag)
        command = 'wsl '
    
    command += '. /opt/ros/kinetic/setup.sh;'
    command += 'rostopic list -b ' + rosbag
    print(command)
    topics = subprocess.check_output(command, shell=True).split()
    return sorted([topic.decode('utf-8') for topic in topics])

def rosbag_to_csv(rosbag, topic):
    command = ''
    csv_file = os.path.splitext(rosbag)[0] + topic.replace('/', '_') + '.csv'

    # for windows(wsl)
    pf = platform.system()
    if pf == 'Windows':
        rosbag = winpath_to_wslpath(rosbag)
        command = 'wsl '
    
    command += '. /opt/ros/kinetic/setup.sh;'
    command += 'rostopic echo -b ' + rosbag + ' -p ' + topic + ' > ' + csv_file
    print(command)
    subprocess.call(command, shell=True)
    return csv_file
# <<<--- Rosbag Method

# --->>> WSL Method
def winpath_to_wslpath(win_path):
    command = 'wsl wslpath -u ' + win_path
    wsl_path = subprocess.check_output(command, shell=True)
    return wsl_path.decode('utf-8').strip('\n')

def wslpath_to_winpath(wsl_path):
    command = 'wsl wslpath -w ' + wsl_path
    win_path = subprocess.check_output(command, shell=True)
    return win_path.decode('utf-8').strip('\n')
# <<<--- WSL Method

# --->>> Other Method
def baglist_from_folder(folder):
    rosbag_list = []
    file_list = os.listdir(folder)
    for file in file_list:
        name, ext = os.path.splitext(file)
        if ext =='.bag':
            rosbag_list.append(folder + '/' + file)
    return rosbag_list
# <<<---

if __name__ == '__main__':
    RosConverterGui()